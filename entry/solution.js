"use strict";

const XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
const http = new XMLHttpRequest();

const usersUrl =
  "https://chaosstack-xu1aet.s3.eu-central-1.amazonaws.com/mapping.json";

const baseUrl = "https://registration.hungary.chaosstack.com/api/qualify";

function getRooms(url) {
  http.open("GET", url, false);
  http.send(null);
  return http.responseText;
}

const users = JSON.parse(getRooms(usersUrl));

function getCommands(url) {
  http.open("GET", url, false);
  http.setRequestHeader(
    "Authorization",
    "c055dcb3a7c2432bbfae14e33e04966582904eeee751771987a7ee6447c39b83"
  );
  http.send(null);
  return http.responseText;
}

const commands = JSON.parse(getCommands(baseUrl + "/user_commands"));

function getBody(action, lights) {
  return {
    "desiredState": action,
    "lights": lights
  }
}

function setLight(url, body) {
  http.open("POST", url, false);
  http.setRequestHeader("Content-Type", "application/json");
  http.setRequestHeader(
    "Authorization",
    "c055dcb3a7c2432bbfae14e33e04966582904eeee751771987a7ee6447c39b83"
  );
  http.send(JSON.stringify(body));
  console.log(body);
  return http.responseText;
}

function setLights(url) {
  commands.userCommands.forEach(command => {
    users.forEach(user => {
      if (user.user === command.user) {
        for (
          let roomNo = 0;
          roomNo < Object.keys(user.rooms).length;
          roomNo++
        ) {
          if (Object.keys(user.rooms)[roomNo] === command.targetRoom) {
            let action = command.action;
            let lights = [];
            for (
              let lightNo = 0;
              lightNo < user.rooms[Object.keys(user.rooms)[roomNo]].length;
              lightNo++
            ) {
              lights.push(user.rooms[Object.keys(user.rooms)[roomNo]][lightNo]);
              }
              let body = getBody(action, lights);
            setLight(url, body);
          }
        }
      }
    });
  });
}

setLights(baseUrl + "/set_lights");
